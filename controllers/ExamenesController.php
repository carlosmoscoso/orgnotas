<?php

namespace app\controllers;

use app\models\Examenes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExamenesController implements the CRUD actions for Examenes model.
 */
class ExamenesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Examenes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Examenes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idExamenes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Examenes model.
     * @param int $idExamenes Id Examenes
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idExamenes)
    {
        return $this->render('view', [
            'model' => $this->findModel($idExamenes),
        ]);
    }

    /**
     * Creates a new Examenes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Examenes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idExamenes' => $model->idExamenes]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Examenes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idExamenes Id Examenes
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idExamenes)
    {
        $model = $this->findModel($idExamenes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idExamenes' => $model->idExamenes]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Examenes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idExamenes Id Examenes
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idExamenes)
    {
        $this->findModel($idExamenes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Examenes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idExamenes Id Examenes
     * @return Examenes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idExamenes)
    {
        if (($model = Examenes::findOne(['idExamenes' => $idExamenes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
