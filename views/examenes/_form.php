<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Examenes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="examenes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'n_preguntas')->textInput() ?>

    <?= $form->field($model, 'nota')->textInput() ?>

    <?= $form->field($model, 'idAlumnos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
