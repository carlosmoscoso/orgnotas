<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Practicas */

$this->title = 'Update Practicas: ' . $model->idPracticas;
$this->params['breadcrumbs'][] = ['label' => 'Practicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPracticas, 'url' => ['view', 'idPracticas' => $model->idPracticas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="practicas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
