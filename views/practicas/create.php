<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Practicas */

$this->title = 'Create Practicas';
$this->params['breadcrumbs'][] = ['label' => 'Practicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="practicas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
