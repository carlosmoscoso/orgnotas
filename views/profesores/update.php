<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesores */

$this->title = 'Update Profesores: ' . $model->idProfesores;
$this->params['breadcrumbs'][] = ['label' => 'Profesores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idProfesores, 'url' => ['view', 'idProfesores' => $model->idProfesores]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
