<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<?php
use yii\helpers\Html;


$this->title = 'Consultas de Seleccion 1';
?>

<div class="site-index">
    
    <div class="jumbotron text-center bg-transparent"
         <h1 class="display-4">Consultas de Seleccion</h1>
    </div>
    
    <div class ="row">
        
        <!-- Primera cajita -->
        <div class="col-sm-6 col-md-4">
            <div class ="card-body tarjeta">
                <h3>Consulta 1</h3>
                <p>Listar las edades de los ciclista (sin repetidos)</p>
                <p>
                    
                    <?= Html::a('Active Recod', ['site/consulta1a'],['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-warning'])?>
                </p>
            </div>
        </div>
        
        <div class="col-sm-6 col-md-4">
            <div class ="card-body tarjeta">
                <h3>Consulta 1</h3>
                <p>Listar las edades de los ciclista (sin repetidos)</p>
                <p>
                    
                    <?= Html::a('Active Recod', ['site/consulta1a'],['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-warning'])?>
                </p>
            </div>
        </div>
    </div>
    
</div>
