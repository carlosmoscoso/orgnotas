<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesores".
 *
 * @property int $idProfesores
 * @property string $nombre
 *
 * @property Diseñan[] $diseñans
 */
class Profesores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProfesores' => 'Id Profesores',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Diseñans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiseñans()
    {
        return $this->hasMany(Diseñan::className(), ['idProfesores' => 'idProfesores']);
    }
}
