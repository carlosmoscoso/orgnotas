<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examenes".
 *
 * @property int $idExamenes
 * @property int $n_preguntas
 * @property int $nota
 * @property int|null $idAlumnos
 *
 * @property Alumnos $idAlumnos0
 */
class Examenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_preguntas', 'nota'], 'required'],
            [['n_preguntas', 'nota', 'idAlumnos'], 'integer'],
            [['idAlumnos'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['idAlumnos' => 'idAlumnos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idExamenes' => 'Id Examenes',
            'n_preguntas' => 'N Preguntas',
            'nota' => 'Nota',
            'idAlumnos' => 'Id Alumnos',
        ];
    }

    /**
     * Gets query for [[IdAlumnos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlumnos0()
    {
        return $this->hasOne(Alumnos::className(), ['idAlumnos' => 'idAlumnos']);
    }
}
