<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $idAlumnos
 * @property string $nombre
 * @property int $grupo
 *
 * @property Examenes[] $examenes
 * @property Practicas[] $practicas
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'grupo'], 'required'],
            [['grupo'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAlumnos' => 'Id Alumnos',
            'nombre' => 'Nombre',
            'grupo' => 'Grupo',
        ];
    }

    /**
     * Gets query for [[Examenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExamenes()
    {
        return $this->hasMany(Examenes::className(), ['idAlumnos' => 'idAlumnos']);
    }

    /**
     * Gets query for [[Practicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPracticas()
    {
        return $this->hasMany(Practicas::className(), ['idAlumnos' => 'idAlumnos']);
    }
}
