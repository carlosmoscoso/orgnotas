<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "practicas".
 *
 * @property int $idPracticas
 * @property string $titulo
 * @property int $grado
 * @property int $idAlumnos
 *
 * @property Diseñan[] $diseñans
 * @property Alumnos $idAlumnos0
 */
class Practicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'practicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'grado', 'idAlumnos'], 'required'],
            [['grado', 'idAlumnos'], 'integer'],
            [['titulo'], 'string', 'max' => 50],
            [['idAlumnos'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['idAlumnos' => 'idAlumnos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPracticas' => 'Id Practicas',
            'titulo' => 'Titulo',
            'grado' => 'Grado',
            'idAlumnos' => 'Id Alumnos',
        ];
    }

    /**
     * Gets query for [[Diseñans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiseñans()
    {
        return $this->hasMany(Diseñan::className(), ['idPracticas' => 'idPracticas']);
    }

    /**
     * Gets query for [[IdAlumnos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlumnos0()
    {
        return $this->hasOne(Alumnos::className(), ['idAlumnos' => 'idAlumnos']);
    }
}
